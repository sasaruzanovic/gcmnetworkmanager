package spoiledmilk.com.gcmnetworkmanager;

/**
 * Created by Sasha Ruzanovic on 21.9.15..
 */
public class WeatherModel {

    private Currently currently;

    public Currently getCurrently() {
        return currently;
    }

    public class Currently {
        long time;
        String summary;
        String icon;
        String precipIntensity;
        String precipProbability;
        String temperature;
        String apparentTemperature;
        String dewPoint;
        String humidity;
        String windSpeed;
        String windBearing;
        String cloudCover;
        String pressure;
        String ozone;

        public long getTime() {
            return time;
        }

        public String getSummary() {
            return summary;
        }

        public String getIcon() {
            return icon;
        }

        public String getPrecipIntensity() {
            return precipIntensity;
        }

        public String getPrecipProbability() {
            return precipProbability;
        }

        public String getTemperature() {
            return temperature;
        }

        public String getApparentTemperature() {
            return apparentTemperature;
        }

        public String getDewPoint() {
            return dewPoint;
        }

        public String getHumidity() {
            return humidity;
        }

        public String getWindSpeed() {
            return windSpeed;
        }

        public String getWindBearing() {
            return windBearing;
        }

        public String getCloudCover() {
            return cloudCover;
        }

        public String getPressure() {
            return pressure;
        }

        public String getOzone() {
            return ozone;
        }
    }

}
