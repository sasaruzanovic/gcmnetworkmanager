package spoiledmilk.com.gcmnetworkmanager;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

/**
 * Created by Sasha Ruzanovic on 18.9.15..
 */
public class AlarmReceiver extends BroadcastReceiver {
    private RequestQueue mRequestQueue;

    @Override
    public void onReceive(final Context context, Intent intent) {

        Log.v("Alarm receive", "Alarm on receive");

        String url = "https://api.forecast.io/forecast/b0e4f1dfb10b3c6b21dba6eaa0957bb6/44.813571,20.512521?units=si";

        GSONRequest request = new GSONRequest(Request.Method.GET, url, WeatherModel.class, new Response.Listener<WeatherModel>() {
            @Override
            public void onResponse(WeatherModel response) {

                // Prepare intent for widget
                Intent intentWd = new Intent(context, WeatherWidget.class);
                intentWd.setAction("android.appwidget.action.APPWIDGET_UPDATE");
                int ids[] = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, WeatherWidget.class));
                intentWd.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);

                intentWd.putExtra("text", response.getCurrently().getTemperature());
                context.sendBroadcast(intentWd);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        mRequestQueue.add(request);
    }
}
