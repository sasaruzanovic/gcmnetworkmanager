package spoiledmilk.com.gcmnetworkmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

import java.util.Calendar;


/**
 * Created by Sasha Ruzanovic on 21.9.15..
 */
public class WeatherWidget extends AppWidgetProvider {

    private String latestTemperature = "";
    private final long periodSecs = 30;
    private long flexSecs = 15;
    private int taskID = 0;
    private final String tag = "periodic  | " + taskID++ + ": " + periodSecs + "s, f:" + flexSecs;  // a unique task identifier

    @Override
    public void onReceive(Context context, Intent intent) {

        latestTemperature = intent.getStringExtra("text");

        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        if (checkGooglePlayServices(context)) {
            startTask(context);
        } else {
            startAlarm(context);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);

        //Stop all tasks when widget is destroyed
        stopTask(context);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int count = appWidgetIds.length;

        for (int i = 0; i < count; i++) {
            int widgetId = appWidgetIds[i];

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            remoteViews.setTextViewText(R.id.textView, latestTemperature);

            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }


    private void stopTask(Context context) {

        GcmNetworkManager.getInstance(context).cancelTask(tag, SyncService.class);
    }

    private void startTask(Context context) {

        PeriodicTask periodic = new PeriodicTask.Builder()
                .setService(SyncService.class)
                .setPeriod(periodSecs)
                .setTag(tag)
                .setPersisted(true)
                .setFlex(flexSecs)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .build();
        GcmNetworkManager.getInstance(context).schedule(periodic);

    }

    private boolean checkGooglePlayServices(Context context) {

        try {
            int version = context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;

            if (version < 7800000) {
                Toast.makeText(context, "Please update google play services", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(context, "Your version of google play services is good", Toast.LENGTH_SHORT).show();
                return true;
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }

    }

    private void startAlarm(Context context) {

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 1, alarmIntent);

    }
}
