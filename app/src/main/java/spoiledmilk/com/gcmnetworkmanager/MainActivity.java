package spoiledmilk.com.gcmnetworkmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "MainActivity";
    private final long periodSecs = 30;
    private long flexSecs = 1;
    private int taskID = 0;
    private final String tag = "periodic  | " + taskID++ + ": " + periodSecs + "s, f:" + flexSecs;  // a unique task identifier


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_start).setOnClickListener(this);
        findViewById(R.id.btn_stop).setOnClickListener(this);

    }

    private void stopTask() {

        GcmNetworkManager.getInstance(MainActivity.this).cancelTask(tag, SyncService.class);
    }

    private void startTask() {
        PeriodicTask periodic = new PeriodicTask.Builder()
                .setService(SyncService.class)
                .setPeriod(periodSecs)
                .setFlex(flexSecs)
                .setTag(tag)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .build();
        GcmNetworkManager.getInstance(MainActivity.this).schedule(periodic);

    }

    private boolean checkGooglePlayServices() {


        try {
            int v = getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;

            if (v < 7800000) {
                Toast.makeText(this, "Please update google play services", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(this, "Your version of google play services is good", Toast.LENGTH_SHORT).show();
                return true;
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }

    }


    private void startAlarm() {

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60 * 1, alarmIntent);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.btn_start: {

                Intent intentWd = new Intent(this, WeatherWidget.class);
                intentWd.setAction("android.appwidget.action.APPWIDGET_UPDATE");
                int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), WeatherWidget.class));
                intentWd.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                sendBroadcast(intentWd);

                // Check if GCM return message for update - not available for now
                if (checkGooglePlayServices()) {
                    startTask();
                } else {
                    startAlarm();
                }
                break;
            }

            case R.id.btn_stop: {

                stopTask();
                break;
            }
        }
    }
}
