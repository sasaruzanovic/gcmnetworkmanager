package spoiledmilk.com.gcmnetworkmanager;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;


/**
 * Created by Sasha Ruzanovic on 17.9.15..
 */
public class SyncService extends GcmTaskService {


    private static String TAG = "SyncService";
    private RequestQueue mRequestQueue;


    @Override
    public int onRunTask(TaskParams taskParams) {

        Log.v(TAG, "Sync start");

        String url = "https://api.forecast.io/forecast/b0e4f1dfb10b3c6b21dba6eaa0957bb6/44.813571,20.512521?units=si";

        GSONRequest request = new GSONRequest(Request.Method.GET, url, WeatherModel.class, new Response.Listener<WeatherModel>() {
            @Override
            public void onResponse(WeatherModel response) {

                // Prepare intent for widget
                Intent intentWeather = new Intent(SyncService.this, WeatherWidget.class);
                intentWeather.setAction("android.appwidget.action.APPWIDGET_UPDATE");
                int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), WeatherWidget.class));
                intentWeather.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);

                intentWeather.putExtra("text", response.getCurrently().getTemperature());
                sendBroadcast(intentWeather);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        mRequestQueue.add(request);

        return GcmNetworkManager.RESULT_FAILURE;

    }

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();

        // Google play services updated


    }


}
